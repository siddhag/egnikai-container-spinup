#!/usr/bin/env bash

EGNIKAI_DOCKER_SPINUP_IMAGE_NAME="egnikai-docker-jdk"

echo "Removing existing docker image..."
sudo docker rmi ${EGNIKAI_DOCKER_SPINUP_IMAGE_NAME}

echo "Building docker image..."
sudo docker build -t ${EGNIKAI_DOCKER_SPINUP_IMAGE_NAME} .
echo "DONE"