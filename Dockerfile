# Use an official Java runtime as a parent image
FROM openjdk:8

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
# ADD ./target /app
# ADD ./bin/start.sh /app
# ADD ./bin/setupenv.sh /app

# Install any needed packages specified in requirements.txt
# RUN pip install -r requirements.txt
RUN apt-get update
RUN apt-get install -y curl
RUN curl -sSL https://get.docker.com/ | sh

# Make port 8080 available to the world outside this container
# EXPOSE 8080

# Define environment variable
# ENV NAME World

# Run our app when the container launches
# CMD ["./start.sh", "."]
