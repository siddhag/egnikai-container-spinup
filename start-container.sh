#!/usr/bin/env bash

EGNIKAI_DOCKER_SPINUP_NAME="egnikai-docker-spike" 
EGNIKAI_DOCKER_SPINUP_IMAGE_NAME="egnikai-docker-jdk"

echo "Removing existing Docker Container..."
sudo docker rm ${EGNIKAI_DOCKER_SPINUP_NAME}

echo "Running new container..."
sudo docker run -it -v /var/run/docker.sock:/var/run/docker.sock --name ${EGNIKAI_DOCKER_SPINUP_NAME} ${EGNIKAI_DOCKER_SPINUP_IMAGE_NAME}
